from telebot import types

def generate_button(url):
    keyboard = types.InlineKeyboardMarkup()
    url_button = types.InlineKeyboardButton(text='View at MAL', url=url)
    keyboard.add(url_button)
    return keyboard