import telebot
from api import Api
from button_generator import generate_button

token = 'tut token'

bot = telebot.TeleBot(token)

api = Api()


@bot.message_handler(commands=['start'])
def cmd_start(message):
    bot.send_message(message.chat.id, text='Welcome')


@bot.message_handler(commands=['random_anime'])
def send_random_anime(message):
    msg = api.message_for_random_anime()
    bot.send_message(message.chat.id, text=msg.text)
    bot.send_photo(message.chat.id, photo=msg.url_picture, reply_markup=generate_button(msg.url))


@bot.message_handler(commands=['random_manga'])
def send_random_manga(message):
    msg = api.message_for_random_manga()
    bot.send_message(message.chat.id, text=msg.text)
    bot.send_photo(message.chat.id, photo=msg.url_picture, reply_markup=generate_button(msg.url))


@bot.message_handler(commands=['find_anime'])
def find_anime(message):
    try:
        for i in range(3):
            msg = api.message_for_find_cmd(type='anime', query=message.text[12:], num=i)
            bot.send_message(message.chat.id, text=msg.text)
            bot.send_photo(message.chat.id, photo=msg.url_picture, reply_markup=generate_button(msg.url))
    except:
        bot.send_message(message.chat.id, text='ivalid title')


@bot.message_handler(commands=['find_manga'])
def find_manga(message):
    try:
        for i in range(3):
            msg = api.message_for_find_cmd(type='manga', query=message.text[12:], num=i)
            bot.send_message(message.chat.id, text=msg.text)
            bot.send_photo(message.chat.id, photo=msg.url_picture, reply_markup=generate_button(msg.url))
    except:
        bot.send_message(message.chat.id, text='ivalid title')

@bot.message_handler(commands=['schedule'])
def get_schedule(message):
    # try:
    msg = api.schedule_message(day=message.text[10:].lower())
    bot.send_message(message.chat.id, text=msg)
    # except:
    #     bot.send_message(message.chat.id, text='invalid day')



if __name__ == "__main__":
    bot.polling(none_stop=True)
