from collections import namedtuple
from jikanpy import Jikan
from random import randint, choice

jikan = Jikan()


Message = namedtuple('Message', ['text', 'url_picture', 'url'])

class Api:

    @staticmethod
    def _get_anime(mal_id):
        return jikan.anime(mal_id)
    @staticmethod
    def _get_manga(mal_id):
        return jikan.manga(mal_id)

    def _get_random_anime(self):
        try:
            mal_id = randint(1, 39000)
            return self._get_anime(mal_id)

        except:
            mal_ids = [1, 20, 30, 37491, 35760, 32281, 11597, 5081, 28025, 1735, 34566]
            mal_id = choice(mal_ids)
            return self._get_anime(mal_id)

    def _get_random_manga(self):
        try:
            mal_id = randint(1, 95215)
            return self._get_manga(mal_id)

        except:
            mal_ids = [1, 2, 1004, 15004, 15006, 3000, 20000, 11, 95210]
            mal_id = choice(mal_ids)
            return self._get_manga(mal_id)

    def _find(self, type, query):
        result = jikan.search(type, query)['result']
        return result

    def _get_finded_result(self, type, query, num):
        result = self._find(type, query)
        return result[num]

    def _get_schedule(self, day):
        result = jikan.schedule(day=day)
        return result

    @staticmethod
    def _get_title_name(result):
        return result['title']

    @staticmethod
    def _get_score(result):
        return result['score']

    def _get_genres(self, result, type=None):
        genres_list = []
        if not type:
            genres = result['genre']
        elif type is 'anime':
            genres = self._get_anime(result['mal_id'])['genre']
        else:
            genres = self._get_manga(result['mal_id'])['genre']
        for i in range(len(genres)):
            genres_list.append(genres[i]['name'])
        return genres_list




    def _get_rank(self, result, type=None):
        if not type:
            return result['rank']
        elif type is 'anime':
            return self._get_anime(result['mal_id'])['rank']
        return self._get_manga(result['mal_id'])['rank']

    def _get_status(self, result, type=None):
        if not type:
            return result['status']
        elif type is 'anime':
            return self._get_anime(result['mal_id'])['status']
        return self._get_manga(result['mal_id'])['status']

    def _get_text(self, result, type=None):
        return 'TITLE: {title}\nSCORE: {score}\nRANK: {rank}\nSTATUS: {status}\nGENRES: {genres}'.format(
            title=self._get_title_name(result),
            score=self._get_score(result),
            rank=self._get_rank(result, type=type),
            status=self._get_status(result, type=type),
            genres=self._get_genres(result, type=type))

    @staticmethod
    def _get_url_picture(result):
        return result.get('image_url', 'http://www.thinktv.org/s/photogallery/img/no-image-available.jpg')

    @staticmethod
    def _get_url(result):
        if result.get('link_canonical'):
            return result['link_canonical']
        return result['url']

    def message_for_random_anime(self):
        result = self._get_random_anime()
        message = Message(text=self._get_text(result), url_picture=self._get_url_picture(result),
                          url=self._get_url(result))
        return message

    def message_for_random_manga(self):
        result = self._get_random_manga()
        message = Message(text=self._get_text(result), url_picture=self._get_url_picture(result),
                          url=self._get_url(result))
        return message

    def message_for_find_cmd(self, type, query, num):
        result = self._get_finded_result(type, query, num)
        message = Message(text=self._get_text(result, type=type), url_picture=self._get_url_picture(result),
                          url=self._get_url(result))
        return message

    def schedule_message(self, day):
        result = self._get_schedule(day=day)
        message = 'SCHEDULE FOR {}: \n'.format(day.upper())
        for i in range(len(result[day])):
            message += result[day][i]['title'] + '\n'
        return message

